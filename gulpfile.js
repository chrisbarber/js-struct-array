const gulp = require('gulp');
const babel = require('gulp-babel');
const saucelabs = require('gulp-saucelabs');
const connect   = require('gulp-connect');


gulp.task('default', () =>
    gulp.src('src/struct-array.js')
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(gulp.dest('dist'))
);

 
// Saucelabs 
gulp.task('saucelabs', () => {
    const config = {
        username: 'chrisbarber',
        urls: ['http://localhost:3000/tests/index.html'],
        framework: 'mocha',
        browsers: [
            {
                browserName: "MicrosoftEdge",
                platform: "Windows 10",
                version: "latest"
            },
            {
                browserName: "internet explorer",
                version: "11",
                platform: "Windows 8.1"
            },
            {
                browserName: "internet explorer",
                version: "10",
                platform: "Windows 8"
            },
            {
                browserName: 'iphone',
                platform: 'OS X 10.8',
                version: '8.1'
            },
            //{
            //    browserName: 'android',
            //    platform: 'Linux',
            //    version: '5.0'
            //},
            {
                browserName: 'firefox',
                platform: 'XP',
                version: '35'
            },
            {
                browserName: 'chrome',
                platform: 'XP',
                version: '41'
            },
            {
                browserName: 'internet explorer',
                platform: 'WIN7',
                version: '11'
            },
            {
                browserName: 'internet explorer',
                platform: 'WIN7',
                version: '10'
            },
            //{
            //    browserName: 'internet explorer',
            //    platform: 'WIN7',
            //    version: '9'
            //},
            //{
            //    browserName: 'internet explorer',
            //    platform: 'WIN7',
            //    version: '8'
            //},
            {
                browserName: 'safari',
                platform: 'OS X 10.8',
                version: '6'
            }
        ],
        onTestSuiteComplete: (status) => {
            if (status) {
                console.log('All tests passed!');
            }
        }
    }
 
    return saucelabs(config);
});
 
// Start local http server 
gulp.task('connect', () => {
    connect.server({ port: 3000, root: './' });
});
 
// Close down the http server 
gulp.task('disconnect', () => {
    connect.serverClose();
});
 
gulp.task('test-saucelabs', ['default', 'connect', 'saucelabs'], () => gulp.start('disconnect'));
