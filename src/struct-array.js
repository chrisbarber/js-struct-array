'use strict';

/**
 * sa.data
 * sa.col_names
 * sa.col_types
 * sa.item_sizes
 * sa.num_rows
 */
function StructArray(sa, little_endian, struct_of_array) {
    this.little_endian = typeof(little_endian) === 'undefined' ? false : little_endian;
    this.struct_of_array = typeof(struct_of_array) === 'undefined' ? false : struct_of_array;

    this.buf = sa.data;
    this.dv = new DataView(this.buf.buffer);
    this.sa = sa;

    var row_sz = 0;
    var has_unicode = false;
    for(var col_idx = 0; col_idx < sa.col_names.length; ++col_idx) {
        row_sz += sa.item_sizes[col_idx];
        if(sa.col_types[col_idx].startsWith('unicode')) {
            has_unicode = true;
        }
    }
    
    this.row_sz = row_sz;

    if(has_unicode) {
        this.decoder = new TextDecoder('utf-8');
    }
};

StructArray.prototype.get_row = function(row_idx) {
    var sa = this.sa;
    var record = {};

    var col_offset = 0;
    for(var col_idx = 0; col_idx < sa.col_names.length; ++col_idx) {
        var col_name = sa.col_names[col_idx];

        var bytes = null;
        var dv = null;
        var begin = null;
        if(this.struct_of_array) {
            begin = col_offset * this.sa.num_rows + sa.item_sizes[col_idx] * row_idx;
        } else {
            begin = col_offset + this.row_sz * row_idx;
        }
        var len = sa.item_sizes[col_idx];
        var end = begin + len;
        bytes = this.buf.slice(begin, end);

        var val = null;
        if(sa.col_types[col_idx].startsWith('string')) {
            var decoded = '';
            for(var i = 0; i < bytes.byteLength && bytes[i]; ++i) {
                decoded += String.fromCharCode(bytes[i]);
            }
            val = decoded;
        } else if(sa.col_types[col_idx].startsWith('unicode')) {
            val = decoder.decode(bytes).replace(/\0/g, '');
        } else {
            if(sa.col_types[col_idx].startsWith('int')) {
                switch(sa.item_sizes[col_idx]) {
                case 1:
                    val = this.dv.getInt8(begin);
                    break;
                case 2:
                    val = this.dv.getInt16(begin, this.little_endian);
                    break;
                case 4:
                    val = this.dv.getInt32(begin, this.little_endian);
                    break;
                }
            } else if(sa.col_types[col_idx].startsWith('uint')) {
                switch(sa.item_sizes[col_idx]) {
                case 1:
                    val = this.dv.getUint8(begin);
                    break;
                case 2:
                    val = this.dv.getUint16(begin, this.little_endian);
                    break;
                case 4:
                    val = this.dv.getUint32(begin, this.little_endian);
                    break;
                }
            } else if(sa.col_types[col_idx].startsWith('float')) {
                switch(sa.item_sizes[col_idx]) {
                case 4:
                    val = this.dv.getFloat32(begin, this.little_endian);
                    break;
                case 8:
                    val = this.dv.getFloat64(begin, this.little_endian);
                    break;
                }
            } else if(sa.col_types[col_idx].startsWith('bool')) {
                val = this.dv.getUint8(begin) ? true : false;
            } else {
                throw 'unsupported column type ' + sa.col_types[col_idx];
            }
        }
        record[col_name] = val;
        col_offset += sa.item_sizes[col_idx];
    }
    return record;
};

StructArray.prototype.get_all = function() {
    var records = [];
    for(var row_idx = 0; row_idx < this.sa.num_rows; ++row_idx) {
        records.push(this.get_row(row_idx));
    }
    return records;
};

StructArray.prototype.lazy_get_all = function() {
    var records_fn = [];
    for(var row_idx = 0; row_idx < this.sa.num_rows; ++row_idx) {
        let row_idx_let = row_idx;
        let self = this;
        records_fn.push(function() {
            return self.get_row(row_idx_let);
        });
    }
    return records_fn;
};
