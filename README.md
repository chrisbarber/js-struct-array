# js-struct-array
[![build status](https://gitlab.com/chrisbarber/js-struct-array/badges/master/build.svg)](https://gitlab.com/chrisbarber/js-struct-array/commits/master)
[![NPM version](https://img.shields.io/npm/v/js-struct-array.svg)](https://www.npmjs.org/package/js-struct-array)

Javascript access to a buffer containing array of structs or struct of arrays
