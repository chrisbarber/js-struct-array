'use strict';

function _base64ToUint8Array(base64) {
    var binary_string =  window.atob(base64);
    var len = binary_string.length;
    var bytes = new Uint8Array( len );
    for (var i = 0; i < len; i++)        {
        bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes;
}

describe('StructArray', function() {

    it('decode array of struct', function() {
        array_of_struct_in.data = _base64ToUint8Array(array_of_struct_in.data);
        var sa = new StructArray(array_of_struct_in);
        chai.assert.deepEqual(sa.get_all(), array_of_struct_out);
    });


    it('decode struct of array', function() {
        struct_of_array_in.data = _base64ToUint8Array(struct_of_array_in.data);
        var sa = new StructArray(struct_of_array_in, false, true);
        chai.assert.deepEqual(sa.get_all(), struct_of_array_out);
    });

});
